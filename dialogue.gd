extends Node2D

#audio control
const FADE_SPD = 5
const FADE_VOICE = 100
#choice control
var listen_pick = 0#timer for the first key : (ie: is holding [action] to listen the current selection
var choice_take = 0#timer for the second key (the one who make the actual choice, ie: hold [left] after and togheter [action] to pick the actual)

const PATH = "res://speech/en/"
const EXT = ".ogg"
const VERIFY_CLASS = "AudioStreamOGGVorbis"

#var selection = ["0000","0001","0002","0003","0004"]#
var selection = {"0000":{"enabled":true}
				,"0001":{"enabled":true,"thinking":3,"open":"0004"}
				,"0002":{"enabled":true}
				,"0003":{"enabled":true}
				,"0004":{"enabled":false}#
				}
var stream_options = {}
var stream_idx = []
var desc = []

#var scene_id
#var active_id = 0

var state = 0
# 0 = idle
# 1 = pressing action (listen the current selection)
# 2 = pressing right (move on next selection and listen it)
# 3 = pressing left (move previous selection and listen it)
# 9 = choice taken

#scene
var current_id
var dialog_lines = []
var active_idx = 0



func _enter_tree():
	load_menu_content()#load the available choices
	if stream_options.size() <= 0:#check if we have at least one option aviable
		error_exit()


func load_menu_content():
	stream_options = {}
	stream_idx = []
	active_idx = 0
	var idx_count = 0
	for opt in selection:
		if selection[opt].has("enabled"):
			if !selection[opt]["enabled"]:continue#this line is not yet enabled
		var stream = str(PATH,opt,"_0",EXT)
		stream = load(stream)
		if stream.get_class() == VERIFY_CLASS:#check if the stream is loaded correctly
			stream_idx.append(opt)
			stream_options[opt] = stream
func error_exit():
	print("error")
	queue_free()
func debug_process(delta):
	var debtext = str("choice: ", listen_pick)
	debtext = debtext+str("\n pick: ", choice_take)
	debtext = debtext+str("\nvolume: ",$mouth.volume_db)
	debtext = debtext+str("\nstate: ",state)
	get_parent().get_node("output").text = debtext

#listen_pick
#choice_take 

func _process(delta):
	debug_process(delta)
	if state == 0:
		$think.volume_db = max(core.voice_volume_min, $think.volume_db-FADE_VOICE*delta)
		if Input.is_action_just_pressed("ui_accept"):
			state = 1
			$think.stream = stream_options[stream_idx[active_idx]]
			$think.volume_db = core.voice_volume_normal
			$think.play()
			listen_pick = 0.5
			choice_take = 0
		elif Input.is_action_just_pressed("r_key"):
			active_idx += 1
			if active_idx >= stream_idx.size():
				active_idx = 0
			state = 2
			current_id = stream_idx[active_idx]
			$think.stream = stream_options[current_id]
			$think.volume_db = core.voice_volume_normal
			$think.play()
			listen_pick = 0.5
			choice_take = 0
		elif Input.is_action_just_pressed("l_key"):
			active_idx -= 1
			if active_idx < 0:
				active_idx = stream_idx.size()-1
			state = 3
			$think.stream = stream_options[stream_idx[active_idx]]
			$think.volume_db = core.voice_volume_normal
			$think.play()
			listen_pick = 0.5
			choice_take = 0
	elif state == 1:
		if Input.is_action_pressed("ui_accept"):
			listen_pick = min(1,listen_pick+FADE_SPD*delta)
		else:
			listen_pick = max(0,listen_pick-FADE_SPD*delta)

		if Input.is_action_pressed("r_key"):choice_take = min(1,choice_take+FADE_SPD*delta)
		elif Input.is_action_pressed("l_key"):choice_take = max(-1,choice_take-FADE_SPD*delta)

		if listen_pick <= 0:state = 0
		if (listen_pick == 1) and (choice_take == 1):get_dialog_lines(stream_idx[active_idx])
		elif (listen_pick == 1) and (choice_take == -1):describe("npc")
		
	elif state == 2:
		if Input.is_action_pressed("r_key"):
			listen_pick = min(1,listen_pick+FADE_SPD*delta)
		else:
			listen_pick = max(0,listen_pick-FADE_SPD*delta)

		if Input.is_action_pressed("ui_accept"):choice_take = min(1,choice_take+FADE_SPD*delta)
		else:choice_take = max(-1,choice_take-FADE_SPD*delta)

		if (listen_pick == 1) and (choice_take == 1):get_dialog_lines(stream_idx[active_idx])
		if listen_pick <= 0:state = 0
	elif state == 3:
		if Input.is_action_pressed("l_key"):
			listen_pick = min(1,listen_pick+FADE_SPD*delta)
		else:
			listen_pick = max(0,listen_pick-FADE_SPD*delta)

		if Input.is_action_pressed("ui_accept"):choice_take = min(1,choice_take+FADE_SPD*delta)
		else:choice_take = max(-1,choice_take-FADE_SPD*delta)

		if (listen_pick == 1) and (choice_take == 1):get_dialog_lines(stream_idx[active_idx])
		if listen_pick <= 0:state = 0
	elif state == 9:
		$think.volume_db = max(core.voice_volume_min, $think.volume_db-FADE_VOICE*delta)
		$mouth.volume_db = core.voice_volume_normal
	elif state == 10:
		$mouth.volume_db = max(core.voice_volume_min, $think.volume_db-FADE_VOICE*delta)
		$mouth.volume_db = core.voice_volume_normal

func describe(what):
	if $think.is_connected("finished",self,"describe"):$think.disconnect("finished",self,"describe")
	print("the what is: ",what)
	if what == "done":
		state = 0
		return
	elif what == "npc":
		state = 10
		#TODO:
		#get npc name
		#get npc status
		#pick the description line from npc status
		
		#or simply cheat and throw the correct file path for demo propouse
		var audiostream = load("res://speech/en/desc_npctest_0.ogg")
		$think.stream = audiostream
		$think.play()
		$think.volume_db = core.voice_volume_normal
		$think.connect("finished",self,"describe", ["done"])
func get_dialog_lines(id):
	state = 9
	var count = 1
	var loading = true
	dialog_lines = []
	while loading:
#		var stream = File.new()
		var path = str(PATH,id,"_",count,EXT)

		var stream = load(path)
		if stream == null:
			loading = false
		elif stream.get_class() == VERIFY_CLASS:
			dialog_lines.append([count,stream])
			count += 1
		else:
			loading = false
#	print("dialog lines: ",dialog_lines)
	var pick_dialog = selection[id]
	if pick_dialog.has("open"):
		var openid = pick_dialog["open"]
		selection[openid]["enabled"] = true 
	if pick_dialog.has("close"):
		var openid = pick_dialog["close"]
		selection[openid]["enabled"] = false
	run_dialogue()

func run_dialogue():
	if $mouth.is_connected("finished",self,"run_dialogue"):$mouth.disconnect("finished",self,"run_dialogue")
	if $think.is_connected("finished",self,"run_dialogue"):$think.disconnect("finished",self,"run_dialogue")
	if dialog_lines.size() <=0:
		state = 0
		load_menu_content()
		return
	var dialog_id = stream_idx[active_idx]
	var line_id = dialog_lines[0][0]
	
	var speech_target = $mouth
	state = 9
	if selection[dialog_id].has("thinking"):
		if line_id == selection[dialog_id]["thinking"]:
			speech_target = $think
			state = 10
	speech_target.stream = dialog_lines[0][1]
	speech_target.play()
	speech_target.volume_db = core.voice_volume_normal
	dialog_lines.remove(0)
	if speech_target.is_connected("finished",self,"run_dialogue"):
		speech_target.disconnect("finished",self,"run_dialogue")
	speech_target.connect("finished",self,"run_dialogue")
