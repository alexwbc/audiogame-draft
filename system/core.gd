extends Node

#nodes
var main

#config
var background_audio = -20
var normal_audio = 0
var sfx_volume_normal = 10
var sfx_volume_min = -10
var voice_volume_normal = 0
var voice_volume_min = -50



#folders
const VOICE_PATH = "res://speech/"
const SFX_PATH = "res://uai/sfx/"

#base audio
var base_audio = {
	
	}

var locale = "en"
var speech_db ={}
var sfx_packed_db = {}


#database:
const database = {"map":"res://level/testmap.json"}# these db will place in the "main" node

#debug stuff
var voutput
var test_data = {}




func _enter_tree():
	main = get_node("/root/main")
	speech_db = load_db("res://system/db/speech_db.json")#media here are just link to be streamed/loaded when needed
	base_audio = load_packed("res://system/db/base_audio.json")#this is always loaded
	debug_stuff()
	for db in database:
		load_database(db, database[db])

func load_database(which, filepath):
	var file = File.new()
	file.open(filepath, file.READ)
	file = parse_json(file.get_as_text())
	if file != null:
		main.set(which,file)

func load_packed(source):
	var file = File.new()
	file.open(source, file.READ)
	file = parse_json(file.get_as_text())
	var content = {}
	for audio in file:
		var filepath = file[audio]
		var checkfile = File.new()
		if checkfile.file_exists(filepath):content[audio] = load(filepath)
	return content
	
func load_db(source):
	var file = File.new()
	file.open(source, file.READ)
	var text = file.get_as_text()
	text = parse_json(text)
	file.close()
	return text

func tran_sfx(node = null):
	if node == null:
		return
	node.get_parent().remove_child(node)
	main.get_node("sfx").add_child(node)
	node.set_owner(main.get_node("sfx"))
	var nope = node.connect("finished", node,"queue_free")

func debug_stuff():
	voutput = get_node("/root/main/output")

	var file = File.new()
	file.open("res://system/db/dev_text.json", file.READ)
	var text = file.get_as_text()
	test_data = parse_json(text)
	file.close()



