extends Node

var sfx = {}
var control_pack
var control
var map = {}

var dialogue_pack
var dialogue

#runtime game
var current_location = "bed"
var current_map = {}
var map_dir = 0

#debug
var map_rotation = 0

func _init_control():
	if dialogue != null:
		dialogue.queue_free()
		dialogue = null
	control = dialogue_pack.instance()
	call_control()

func _init_dialogue():
	if control != null:
		control.disconnect("tree_exited",self,"call_control")
		control.queue_free()
		control = null
	if dialogue != null:dialogue.queue_free()
	dialogue = dialogue_pack.instance()
#	dialogue_pack = load("res://dialogue.tscn")
	
	call_dialogue()



func _enter_tree():
	$dialog_run.connect("pressed",self,"button_dialog")
	$world_run.connect("pressed",self,"button_world")
	dialogue_pack = load("res://dialogue.tscn")
	control_pack = load("res://control.tscn")

#	_init_control()
#	_init_dialogue()
#	sfx["transition"] = $sfx/transation

func call_dialogue():
	if dialogue != null:
		dialogue.queue_free()
	dialogue = dialogue_pack.instance() as Node2D
	add_child(dialogue)


func call_control():

	$rot_cont.text = str(map_dir)
	if control != null:
		control.queue_free()
		control = null
	control = control_pack.instance() as Node2D
	control.connect("tree_exited",self,"call_control")
	call_deferred("add_child", control)
	

func _on_test_control_pressed():
	call_control()
	
	$test_control.disabled = true
	$output.text = ""

func button_dialog():
	$dialog_run.disabled = true
	$world_run.disabled = false

	_init_dialogue()
func button_world():

	$dialog_run.disabled = false
	$world_run.disabled = true
	_init_control()
