extends Node2D

#nodes
onready var mouth = $mouth
onready var mouth_fade = $mouth/fade
onready var sfx = $sfx
onready var sfx_fade = $sfx/fade

#config
const FADE_VOICE = 2
var decision_timeout = [0,1.5]
var go_for_choice = false

var temporary_data = {
	"pick_l":"res://speech/en/test_pick_water.ogg"
	,"bg_l":"res://uai/box/choice_water.ogg"
	,"pick_r":"res://speech/en/test_pick_fire.ogg"
	,"bg_r":"res://uai/box/choice_fire.ogg"
	,"going":"res://sfx/goingwalk.wav"
	,"pick_act":"res://speech/en/test_hold_action.ogg"
#	,"act_l":"res://sfx/percep.ogg"
	,"act_r":"res://sfx/harness-rustle.ogg"
#	,"pick_act":"res://"
#	,"bg_act":"res://"
	}


var data = {}


var coming_pos = 0#1,2,3,4,5,6

var key_press = Vector2(0,0)
var choice_dir = Vector3(0,0,0)

var available_path =["block","block","block"]

var deciding = null
var deciding_old = "neutral"

const CHOICE_SPD = 1
const DEADZONE = 0.2


var speech_current = "silence"
var speech_new = ["silence",false]

const rot_arr = {
	5:0
	,4:2
	,3:2
	,2:1
	,1:1
	,0:0
	,-1:0
	,-2:-1
	,-3:-1
	,-4:-2
	,-5:-2}
const inv_rot = {
	5:2
	,4:1
	,3:0
	,2:5
	,1:4
	,0:-3
	,-1:-4
	,-2:-5
	,-3:0
	,-4:-1
	,-5:-2}


var state = 0
#		0 = neutral in the middle
#		1 = direction choice
#		2 = action choice
#		3 = choice taken

#debug stuff
onready var sprite = load("res://level/ref_guy.png")
var test_arr = [1,2,3,4,5,6,7]
func _ready():
	set_room()
	
#	debug
#	$testmap.rotation_degrees = core.main.map_rotation

#func set_map(which, coming_position):

#func rotate_map(data,rot):
#	data.invert()
#
#	return data

func left_rotate(arr, N):
	print("the arr is: ",arr)
	var map_rot = core.main.map_dir
	N = rot_arr[map_rot]
	#
	var L = arr.size()
	var rotated_array = []
	for i in range(N, N + L):
		rotated_array.append(arr[i % L])
	return rotated_array  

func set_room():
	var loc = core.main.current_location
	var map = core.main.map
	get_node(str("testmap/",loc)).texture = sprite
	var target = ["block","block","block"]
	#
	print("map is: ", map)
	target = left_rotate(map[loc]["exits"], coming_pos)
	available_path = target
	
	if available_path[0] != "block":
		data["pick_l"] = load(str("res://level/map/pickdesc_",available_path[0],".ogg"))
	if available_path[1] != "block":
		data["act_l"] = load(str("res://level/map/pickdesc_",available_path[1],".ogg"))
	if available_path[2] != "block":
		data["pick_r"] = load(str("res://level/map/pickdesc_",available_path[2],".ogg"))
	
	
	#debug
	$testmap.rotation_degrees = core.main.map_dir*60
	core.main.get_node("select").text = str(available_path)
	for p in $testmap.get_children():
		if target.has(p.name) or (core.main.current_location == p.name):
			p.rotation_degrees = core.main.map_rotation-$testmap.rotation_degrees
			p.show()
		else:
			p.hide()
	print(core.main.current_location)
	data["local"] = load(str("res://level/map/curdesc_",core.main.current_location,".ogg"))
	say("local")

func temporary_load_data():
	for d in temporary_data:
		data[d] = load(temporary_data[d])

func _enter_tree():
	temporary_load_data()
	print("entered")



func _input(event):
	if event.get_class() == "InputEventKey":
		key_press.x = Input.get_action_strength("r_key") - Input.get_action_strength("l_key")


func say(what):#, immediatly = false):
	if (what == "silence") or !data.has(what):
		mouth.stop()
		return
		
	mouth.volume_db = core.voice_volume_normal
	mouth.stream = data[what]
	mouth.play()

func sound(what):

	if sfx.playing:
		sfx_fade.stream = sfx.stream
		sfx_fade.volume_db = sfx.volume_db
		sfx_fade.play(mouth.get_playback_position())
	if (what == "silence") or !data.has(what):
		sfx.stop()
	else:
		sfx.stream = data[what]
		sfx.play()

func proc_neutral(delta):
	if key_press.x > 0:
		deciding = "right"
		state = 1
		choice_dir = Vector3(0,0,0.5)
		$choice_bg.stream = data["bg_r"]
		$choice_bg.play()
		$choice_bg.volume_db = core.sfx_volume_min
		speech_new = ["pick_r",false]
	elif key_press.x < 0:
		deciding = "left"
		state = 1
		choice_dir = Vector3(0.5,0,0)
		$choice_bg.stream = data["bg_l"]
		$choice_bg.play()
		$choice_bg.volume_db = core.sfx_volume_min
		speech_new = ["pick_l",false]
	elif Input.is_action_pressed("ui_accept"):
		speech_new = ["pick_act",false]
		deciding = "local"
		state = 2
		choice_dir = Vector3(0,0.5,0)




func proc_direction(delta):
	var choice = Input.is_action_pressed("ui_accept")





#pick choice dynamics
	if !go_for_choice and choice:#action just pressed, reset timer, play sfx
		go_for_choice = true
		decision_timeout[0] = decision_timeout[1]
		sound("going")
	if go_for_choice:
		if !choice:#choice button released, reset
			decision_timeout[0] = decision_timeout[1]
			sound("silence")
			go_for_choice = false
		else:
			decision_timeout[0] -= 1*delta
		if decision_timeout[0]<= 0:

			var path = "block"
			if deciding == "left":# and (available_path[0] == "block"):
				path = available_path[0]
			elif deciding == "right":# and (available_path[2] == "block"):
				path = available_path[2]
			if path == "block":
				decision_timeout[0] = decision_timeout[1]
				core.voutput.text = str("ERRROR, no return path!")
				return

#			print("deciding is: ",deciding)
			choice_taken(deciding,path)
			say("silence")
			core.tran_sfx(sfx)
			state = 3
			return
#########


	if deciding == null:
		state = 0
		choice_dir = Vector3(0,0,0)
		$choice_bg.stop()
		return

	var bg_volume = $choice_bg.volume_db
	if deciding == "left":
		var value = choice_dir.x
		value -= key_press.x*CHOICE_SPD*delta
		value -= 0.5*delta
		if value < DEADZONE:
			deciding = null
			speech_new[0] = "silence"
			speech_new[1] = true
			return
		choice_dir.x = min(value,1)
		$choice_bg.volume_db = lerp(core.sfx_volume_min,core.sfx_volume_normal, choice_dir.x)

	elif deciding == "right":
		var value = choice_dir.z
		value += key_press.x*CHOICE_SPD*delta
		value -= 0.5*delta
		if value < DEADZONE:
			deciding = null
			speech_new[0] = "silence"
			speech_new[1] = true
			return
		choice_dir.z = min(value,1)
		$choice_bg.volume_db = lerp(core.sfx_volume_min,core.sfx_volume_normal, choice_dir.z)




func proc_local(delta):

	if deciding == null:
		state = 0
		choice_dir = Vector3(0,0,0)
		$choice_bg.stop()
		return



	var holding = Input.is_action_pressed("ui_accept")
	var value = choice_dir.y
	
	if holding:
		value += CHOICE_SPD*delta
	else:
		value -= CHOICE_SPD*delta


	deciding = "neutral"
	if key_press.x > 0.5:
		deciding = "action"#right
	elif key_press.x < -0.5:
		deciding = "step_back"#left



	if deciding != deciding_old:#there's a chance of decision
		decision_timeout[0] = decision_timeout[1]
		deciding_old = deciding
		if deciding == "step_back":
			sound("going")
		elif deciding == "action":
			sound("act_r")
		else:
			sound("silence")
	elif deciding != "neutral":
		decision_timeout[0] -= 1*delta
		if decision_timeout[0] <= 0:
			var decidefor = str("extraopt_",deciding)
			var path = "action"
			if deciding == "step_back":# and (available_path[1] == "block"):#check if the path is open
				path = available_path[1]
				decidefor = "step_back"
			if path == "block":
				decision_timeout[0] = decision_timeout[1]
				core.voutput.text = str("ERRROR, no return path!")
				return
			choice_taken(decidefor,path)
			say("silence")
			core.tran_sfx(sfx)
			state = 3
			return



	if value < DEADZONE:
		deciding = null
		speech_new[0] = "silence"
		speech_new[1] = true
		return
		
	choice_dir.y = min(value,1)




func _process(delta):
	if Input.is_action_just_pressed("ui_home"):
		$testmap.rotation_degrees += 60
	if Input.is_action_just_pressed("ui_end"):
		$testmap.rotation_degrees -= 60
		
	if Input.is_action_just_pressed("ui_page_up"):
		test_arr = left_rotate(test_arr, 1)
	if Input.is_action_just_pressed("ui_page_down"):
		test_arr = left_rotate(test_arr, -1)
	if state == 0:
		proc_neutral(delta)
	elif state == 1:
		proc_direction(delta)
	elif state == 2:
		proc_local(delta)
	elif state == 3:
		modulate.a -= 1*delta
		if modulate.a <= 0:
			queue_free()

	if speech_current != speech_new[0]:
		if speech_new[1] and mouth.playing:#there's a request for fading out the mouth (instad of simply drop it)
			#let's copy the current stream/volume/position to the "fader"
			mouth_fade.stream = mouth.stream
			mouth_fade.volume_db = mouth.volume_db
			mouth.stop()
			mouth_fade.play(mouth.get_playback_position())
		say(speech_new[0])
		speech_current = speech_new[0]
		speech_new[1] = false
	if mouth_fade.playing:
		mouth_fade.volume_db -= (abs(core.voice_volume_min)*FADE_VOICE)*delta
		if mouth_fade.volume_db <= core.voice_volume_min:
			mouth_fade.stop()
	if sfx_fade.playing:
		sfx_fade.volume_db -= (abs(core.sfx_volume_min)*FADE_VOICE)*delta
		if sfx_fade.volume_db <= core.sfx_volume_min:
			sfx_fade.stop()



	$left_bar.value = choice_dir.x*100
	$action_bar.value = choice_dir.y*100
	$right_bar.value = choice_dir.z*100
	core.main.get_node("timeout").text = str(decision_timeout[0])




func choice_taken(deciding, path):
#	print("O: ",deciding)

	if path != "action" and path != "block":
		core.main.current_location = path

#	core.main.get_node("test_control").disabled = false
	core.main.control = null
	core.voutput.text = str("player has chosen: " ,deciding)
	var dir = core.main.map_dir
	if deciding == "left":
		dir += 1
		if dir > 5:dir = 0
	elif deciding == "right":
		dir -= 1
		if dir < -5:dir = 0
	elif deciding == "step_back":
#		print("got step back")
		dir = inv_rot[dir]
#	min(maxv, max(minv, val))
#	core.main.map_dir = min(3, max(-3, dir))
	core.main.map_dir = dir
	
	#debug
#	if deciding == "right":
#		core.main.map_rotation -= 60
#	elif deciding == "left":
#		core.main.map_rotation += 60
#	elif  deciding == "step_back":
#		core.main.map_rotation = 180-core.main.map_rotation
